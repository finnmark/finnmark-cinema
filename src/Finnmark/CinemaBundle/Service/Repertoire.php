<?php
namespace Finnmark\CinemaBundle\Service;

use Doctrine\ORM\EntityManager;

class Repertoire {
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    protected $_em;
    
    /**
     * 
     * @var Cinema
     */
    protected $_cinema;
    
    public function __construct( EntityManager $em, Cinema $cinema, EntityManagerExtender $extender ) {
        $this->_em = $em;
        $this->_cinema = $cinema;
        //it is enough that extender is created
    }
    
    /**
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getManager() {
        return $this->_em;
    }
    
    /**
     * 
     * @return \Finnmark\CinemaBundle\Service\Cinema
     */
    public function _getCinema() {
        return $this->_cinema;
    }
    
    /**
     * 
     * @param mixed $cinema
     * @return \Finnmark\CinemaBundle\Entity\Cinema
     */
    protected function toEntity( $cinema ) {
        if( !is_object( $cinema ) ) {
            $cinema = $this->_getCinema()->getCinema( $cinema );
        }
        return $cinema;
    }
    
    /**
     * 
     * @param mixed $date
     * @return \DateTime
     */
    protected function toDateTime( $date ) {
        if( !($date instanceof \DateTime) ) {
            $date = new \DateTime($date);
        }
        return $date;
    }
    
    /**
     * @param \DateTime $date
     * @return array
     */
    protected function _preloadMovies( $cinema, $date ) {
        $qb = $this->getManager()->createQueryBuilder();
        $qb->select('m')
            ->from('FinnmarkCinemaBundle:Movie', 'm')
            ->join('m.movieVersions', 'mv')
            ->join('mv.seances', 's')
            ->join('s.room', 'r')
            ->where('DATE(s.at)=:at')->setParameter(':at', $date)
            ->andWhere('r.cinema=:cinema')->setParameter(':cinema', $cinema)
        ;
        return $qb->getQuery()->getResult();
    }
    
//     protected function _preloadSeances( $cinema, $date ) {
//         $qb = $this->getManager()->createQueryBuilder();
//         $qb->select('s')
//             ->from('FinnmarkCinemaBundle:Seance', 's')
//             ->join('s.room', 'r')
//             ->where('DATE(s.at)=:at')->setParameter(':at', $date)
//             ->andWhere('r.cinema=:cinema')->setParameter(':cinema', $cinema)
//         ;
//         var_dump(count($qb->getQuery()->getResult()));
//     }
    
    /**
     * 
     * @param mixed $cinema
     * @param mixed $date
     */
    public function getByDay( $cinema, $date ) {
        $cinema = $this->toEntity($cinema);
        $date = $this->toDateTime($date);
        
        $qb = $this->getManager()->createQueryBuilder();
        $qb->select('mv')
            ->from('FinnmarkCinemaBundle:MovieVersion', 'mv')
            ->join('mv.movie', 'm')
            ->join('mv.seances', 's')
            ->join('s.room', 'r')
            ->where('DATE(s.at)=:at')->setParameter(':at', $date)
            ->andWhere('r.cinema=:cinema')->setParameter(':cinema', $cinema)
            ->orderBy('m.title', 'ASC')
        ;
        $movieVersions = $qb->getQuery()->getResult();
        if( count( $movieVersions ) > 0 ) {
            /*
             * Preload movies with a single query. The result is not used here but in view the references to 
             * movies don't hit DB with single queries. Doctrine cache is used instead.
             */
            $this->_preloadMovies( $cinema, $date );
//             $this->_preloadSeances( $cinema, $date ); //will not work in this direction (fetching many form one)
        }
        return $movieVersions;
    }
}