<?php
namespace Finnmark\CinemaBundle\Service\Booking;

use Finnmark\CinemaBundle\Service\Entity\Constructor;
use Finnmark\CinemaBundle\Service\ConsistencyCheck\ConsistencyCheck;
use Finnmark\CinemaBundle\Exception\ConsistencyCheck\SeanceSeatMatchFailed;
use Finnmark\CinemaBundle\Entity\Seance;
use Finnmark\CinemaBundle\Service\Entity\Loader;
use Finnmark\CinemaBundle\Entity\Reservation;
use Doctrine\ORM\EntityManager;

/**
 * A service conductiong basic operations connected with booking.
 * 
 * @author PPH
 *
 */
class Booking implements BookingInterface {
    /**
     * @var \Finnmark\CinemaBundle\Service\Entity\Constructor
     */
    protected $_ec;
    
    /**
     * 
     * @var ConsistencyCheck
     */
    protected $_cc;
    
    protected $_entityLoader;
    
    protected $_entityManager;
    
    public function __construct( Constructor $ec, ConsistencyCheck $cc, Loader $entityLoader, EntityManager $manager ) {
        $this->_ec = $ec;
        $this->_cc = $cc;
        $this->_entityLoader = $entityLoader;
        $this->_entityManager = $manager;
    }
    
    /**
     * 
     * @return \Finnmark\CinemaBundle\Service\Entity\Constructor
     */
    public function getEntityConstructor() {
        return $this->_ec;
    }
    
	/* (non-PHPdoc)
	 * @see \Finnmark\CinemaBundle\Service\Booking\BookingInterface::book()
	 */
	public function book(\Finnmark\CinemaBundle\Entity\Seance $seance, \Finnmark\CinemaBundle\Entity\Seat $seat, \Finnmark\CinemaBundle\Entity\Booking $booking) {
	    if( !$this->_cc->checkSeanceSeat($seance, $seat) ) {
	        throw new SeanceSeatMatchFailed("Seance id={$seance->getId()} and Seat id={$seat->getId()} don't match.");
	    }
        /*
         * First check if this reservation is already done.
         */
        $criteria = array(
            'seat'    => $seat,
            'seance'  => $seance,
            'booking' => $booking,
        );
        $reservation = $this->_entityManager->getRepository('\Finnmark\CinemaBundle\Entity\Reservation')->findOneBy($criteria);
        if( !($reservation instanceof \Finnmark\CinemaBundle\Entity\Reservation) ) {
            $reservation =  $this->getEntityConstructor()->createEntity( 'Reservation' );
            $reservation->setSeat( $seat );
            $reservation->setSeance( $seance );
            $reservation->setBooking( $booking );
        }
	    return $reservation;
	}
	
    /**
     * 
     * @param \Finnmark\CinemaBundle\Entity\Seat $seat
     * @param \Finnmark\CinemaBundle\Entity\Seance $seance
     * @param \Finnmark\CinemaBundle\Entity\Booking $booking
     * @return boolean
     */
    public function remove(\Finnmark\CinemaBundle\Entity\Seance $seance, \Finnmark\CinemaBundle\Entity\Seat $seat, \Finnmark\CinemaBundle\Entity\Booking $booking ) {
        $reservation = $this->getReservation($seat, $seance);
        if( ($reservation instanceof \Finnmark\CinemaBundle\Entity\Reservation) && $reservation->getBooking()->getId() == $booking->getId()  ) {
            $this->_entityManager->remove( $reservation );
            return true;
        }
        return false;
    }
	

	/* (non-PHPdoc)
	 * @see \Finnmark\CinemaBundle\Service\Booking\BookingInterface::create()
	 */
	public function create( Seance $seance ) {
        $booking = $this->getEntityConstructor()->createEntity( 'Booking' );
        $booking->setSeance( $seance );
        return $booking;
	}
	
	/**
	 * 
	 * @param \Finnmark\CinemaBundle\Entity\Seat $seat
	 * @param \Finnmark\CinemaBundle\Entity\Seance $seance
	 * @return \Finnmark\CinemaBundle\Entity\Reservation
	 */
	public function getReservation( \Finnmark\CinemaBundle\Entity\Seat $seat, \Finnmark\CinemaBundle\Entity\Seance $seance ) {
	    $id = array(
	        'seance' => $seance->getId(),
	        'seat' => $seat->getId(),
	    );
        $reservation = $this->_entityLoader->findReservation($id);
        return $reservation;
	}
    
    /**
     * 
     * @param \Finnmark\CinemaBundle\Entity\Booking $booking
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getBookingReservations( \Finnmark\CinemaBundle\Entity\Booking $booking ) {
        return $booking->getReservations();
    }
	
	/**
	 * 
	 * @param \Finnmark\CinemaBundle\Entity\Seat $seat
	 * @param \Finnmark\CinemaBundle\Entity\Seance $seance
	 * @return boolean
	 */
	public function isBooked( \Finnmark\CinemaBundle\Entity\Seat $seat, \Finnmark\CinemaBundle\Entity\Seance $seance ) {
        $reservation = $this->getReservation($seat, $seance);
        if( $reservation instanceof Reservation ) {
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @param \Finnmark\CinemaBundle\Entity\Seat $seat
     * @param \Finnmark\CinemaBundle\Entity\Seance $seance
     * @param \Finnmark\CinemaBundle\Entity\Booking $booking
     * @return boolean
     */
    public function isBookedByUser(\Finnmark\CinemaBundle\Entity\Seat $seat, \Finnmark\CinemaBundle\Entity\Seance $seance, \Finnmark\CinemaBundle\Entity\Booking $booking) {
        $reservation = $this->getReservation($seat, $seance);
        if( $reservation instanceof Reservation ) {
            $reservationBooking = $reservation->getBooking();
            if( $reservationBooking instanceof \Finnmark\CinemaBundle\Entity\Booking && $reservationBooking->getId() == $booking->getId() ) {
                return true;
            }
            return false;
        }
        return false;
    }
}