<?php
namespace Finnmark\CinemaBundle\Service\Booking;

use Symfony\Component\HttpFoundation\Session\Session;
use Finnmark\CinemaBundle\Service\Booking\Booking as BookingService;
use Doctrine\Common\Persistence\ObjectManager;
use Finnmark\CinemaBundle\Exception\EntityConstructor\UnauthorisedBookingAccess;
use Finnmark\CinemaBundle\Exception\EntityConstructor\BookingDoesntExist;
use Finnmark\CinemaBundle\Entity\Seance;
use Finnmark\CinemaBundle\Entity\Seat;
use Finnmark\CinemaBundle\Entity\Booking as BookingEntity;

/**
 * Object responsible for containing all opened bookings for current user.
 * @author Michał Kocztorz
 */
class Vault {
    
    protected $_session;
    
    protected $_bookingService;
    
    /**
     * 
     * @var ObjectManager
     */
    protected $_om;
    
    protected $_bookingList = array();
    
    public function __construct( Session $session, BookingService $bookingService, ObjectManager $manager ) {
        $this->_session = $session;
        $this->_bookingService = $bookingService;
        $this->_om = $manager;
        $this->_bookingList = $session->get('bookingVault.list', array());
    }
    
    protected function _sessionSet() {
        $this->_session->set('bookingVault.list', $this->_bookingList);
    }
    
    /**
     * Get previous or create new booking entity.
     * @param Seance $seance
     * @throws BookingDoesntExist
     * @throws UnauthorisedBookingAccess
     * @return \Finnmark\CinemaBundle\Entity\Booking
     */
    public function getBooking( Seance $seance ) {
        if( !isset( $this->_bookingList[$seance->getId()] ) ) {
            //for this seance there is no booking set yet
            return $this->_createBooking( $seance );
        }
        else {
            $booking = $this->_om->getRepository('\Finnmark\CinemaBundle\Entity\Booking')->find( $this->_bookingList[$seance->getId()] );
            if( $booking instanceof BookingEntity) {
                //test if booking is active
                return $booking;
            }
            else {
                //the id is wrong, no data in database
                //ToDo: should log warning - this id should have been in db
                return $this->_createBooking( $seance );
                
//                 throw new BookingDoesntExist("Booking id={$id} is not in database.");
            }
        }
    }
    
    /**
     * Creates booking entity for seance and saves id in session.
     * @param Seance $seance
     * @return Booking
     */
    protected function _createBooking( Seance $seance ) {
        $booking = $this->_bookingService->create( $seance );
        $this->_om->persist( $booking );
        $this->_om->flush( $booking );
        $this->_bookingList[ $seance->getId() ] = $booking->getId();
        $this->_sessionSet();
        return $booking;
    }
    
    /**
     * 
     * @param \Finnmark\CinemaBundle\Entity\Seance $seance
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSeanceReservations( Seance $seance ) {
        $booking = $this->getBooking($seance);
        return $this->_bookingService->getBookingReservations($booking);
    }
    
    /**
     * 
     * @param \Finnmark\CinemaBundle\Entity\Seance $seance
     * @param \Finnmark\CinemaBundle\Entity\Seat $seat
     * @return \Finnmark\CinemaBundle\Entity\Reservation
     */
    public function book( Seance $seance, Seat $seat ) {
        return $this->_bookingService->book( $seance, $seat, $this->getBooking($seance) );
    }
    
    /**
     * 
     * @param \Finnmark\CinemaBundle\Entity\Seance $seance
     * @param \Finnmark\CinemaBundle\Entity\Seat $seat
     * @return bool
     */
    public function remove( Seance $seance, Seat $seat  ) {
        return $this->_bookingService->remove($seance, $seat, $this->getBooking($seance) );
    }
}