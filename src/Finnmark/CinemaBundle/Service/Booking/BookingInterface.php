<?php
namespace Finnmark\CinemaBundle\Service\Booking;

interface BookingInterface {
    /**
     * 
     * @param \Finnmark\CinemaBundle\Entity\Seance $seance
     * @param \Finnmark\CinemaBundle\Entity\Seat $seat
     * @param \Finnmark\CinemaBundle\Entity\Booking $booking
     * 
     * @return \Finnmark\CinemaBundle\Entity\Reservation
     */
    public function book(\Finnmark\CinemaBundle\Entity\Seance $seance, \Finnmark\CinemaBundle\Entity\Seat $seat, \Finnmark\CinemaBundle\Entity\Booking $booking);
    
    /**
     * @return \Finnmark\CinemaBundle\Entity\Booking
     */
    public function create( \Finnmark\CinemaBundle\Entity\Seance $seance );
}