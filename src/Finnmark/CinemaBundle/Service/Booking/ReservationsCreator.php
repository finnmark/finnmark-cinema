<?php
namespace Finnmark\CinemaBundle\Service\Booking;

use Finnmark\CinemaBundle\Service\Entity\Loader;
use Finnmark\CinemaBundle\Service\Booking\Vault;
use Finnmark\CinemaBundle\Service\Booking\Booking;
use Doctrine\ORM\EntityManager;

/**
 * Description of ReservationsCreator
 *
 * @author PPH
 */
class ReservationsCreator {
    /**
     *
     * @var Loader
     */
    protected $_loader;
    
    /**
     *
     * @var Vault
     */
    protected $_vault;
    
    /**
     *
     * @var Booking
     */
    protected $_booking;
    
    /**
     *
     * @var EntityManager 
     */
    protected $_manager;
    
    public function __construct( Loader $loader, Vault $vault, Booking $booking, EntityManager $manager ) {
        $this->_loader = $loader;
        $this->_vault = $vault;
        $this->_booking = $booking;
        $this->_manager = $manager;
    }
    
    /**
     * 
     * @param string $seance
     * @param array $seats
     */
    public function makeReservations( $seance, $seats ) {
        $this->_manager->beginTransaction();
        try {
            $seance = $this->_loader->findSeance( $seance );
//            $booking = $this->_vault->getBooking( $seance );
            
            $previousReservations = $this->_vault->getSeanceReservations( $seance );
            
            foreach( $seats as $seatId ) {
                $seat = $this->_loader->findSeat( $seatId );
                $reservation = $this->_vault->book( $seance, $seat );
                if( $previousReservations->contains($reservation) ) {
                    $previousReservations->removeElement( $reservation );
                }
                $this->_manager->persist( $reservation );
            }
            
            /*
             * Now previous reservations contains reservations that were not
             * sent in new list but were there before. 
             * They need to be deleted from DB.
             */
            foreach( $previousReservations as $pr ) {
                $this->_vault->remove($pr->getSeance(), $pr->getSeat());
            }
            
            $this->_manager->flush();
            $this->_manager->commit();
            return true;
        } catch (\Exception $ex) {
            $this->_manager->rollback();
        }
        return false;
    }
}
