<?php
namespace Finnmark\CinemaBundle\Service\Entity;

use Doctrine\ORM\EntityManager;
use Finnmark\CinemaBundle\Exception\EntityConstructor\UnknownEntityName;
use Finnmark\CinemaBundle\Exception\Entity\UnknownEntityId;

class Loader {
    /**
     * 
     * @var EntityManager
     */
    protected $_em;
    
    protected $_entityNamespace;
    
    public function __construct( EntityManager $em, $entityNamespace ) {
        $this->_em = $em;
        $this->_entityNamespace = $entityNamespace;
    }
    
    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public function getManager() {
        return $this->_em;
    }
    
    protected function _find( $id, $name, $returnEmpty = true ) {
        $name = $this->_entityNamespace . $name;
        if( class_exists($name) ) {
            $seance = $this->getManager()->getRepository($name)->find( $id );
            if( $returnEmpty ) {//return whatever manager returns
                return $seance;
            }
            if( $seance instanceof $name ) {
                return $seance;
            }
            else {
                throw new UnknownEntityId("Seance id={$id} not found");
            }
        }
        else {
            throw new UnknownEntityName("Entity {$name} not found");
        }
    }
    
    public function findSeance( $id ) {
        $name = $this->_entityNamespace . 'Seance';
        if( class_exists($name) ) {
            $seance = $this->getManager()->getRepository($name)->find( $id );
            if( $seance instanceof $name ) {
                return $seance;
            }
            else {
                throw new UnknownEntityId("Seance id={$id} not found");
            }
        }
        else {
            throw new UnknownEntityName("Entity {$name} not found");
        }
    }
    
    /**
     * 
     * @param unknown $id
     * @return \Finnmark\CinemaBundle\Entity\Reservation
     */
    public function findReservation( $id ) {
        return $this->_find($id, 'Reservation');
    }

    /**
     * 
     * @param string $id
     * @return \Finnmark\CinemaBundle\Entity\Seat
     */
    public function findSeat( $id ) {
        return $this->_find($id, 'Seat');
    }
}