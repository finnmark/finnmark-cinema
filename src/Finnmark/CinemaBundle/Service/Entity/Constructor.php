<?php
namespace Finnmark\CinemaBundle\Service\Entity;

use Doctrine\ORM\EntityManager;
use Finnmark\CinemaBundle\Exception\EntityConstructor\UnknownEntityName;

class Constructor {
    /**
     * 
     * @var EntityManager
     */
    protected $_em;
    
    protected $_entityNamespace;
    
    public function __construct( EntityManager $em, $entityNamespace ) {
        $this->_em = $em;
        $this->_entityNamespace = $entityNamespace;
    }
    
    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public function getManager() {
        return $this->_em;
    }
    
    /**
     * Returns unmanaged entity.
     * @param string $name
     * @return stdClass
     */
    public function createEntity( $name ) {
        $name = $this->_entityNamespace . $name;
        if( class_exists($name) ) {
            return new $name;
        }
        throw new UnknownEntityName("Entity $name not found");
    }
}