<?php
namespace Finnmark\CinemaBundle\Service;

use Finnmark\CinemaBundle\Entity\MovieVersion;
use Symfony\Component\Templating\Helper\AssetsHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\Asset\PathPackage;
class MovieMedia {
    /**
     * 
     * @var AssetsHelper
     */
    protected $_assetsHelper;
    
    protected $_container;

    /**
     * Is injected with configured AssetsHelper
     * @param AssetsHelper $assetsHelper
     */
    public function __construct( AssetsHelper $assetsHelper, ContainerInterface $container = null ) {
        $this->_assetsHelper = $assetsHelper;
        $this->_container = $container;
    }
    /**
     * Get path 
     * @param MovieVersion $mv
     * @return string
     */
    public function getCoverURL( MovieVersion $mv ) {
        $base = "";
        if( $this->_container instanceof ContainerInterface ) {
//             var_dump(get_class($this->_container->get('router')->getContext()));
            /* @var $context \Symfony\Component\Routing\RequestContext */
            $context = $this->_container->get('router')->getContext();
            $base = $context->getScheme() . '://' . $context->getHost();
        }
        $url = $base . $this->_assetsHelper->getUrl('version/' . $mv->getId() . '/cover.jpg');
//         var_dump($this->remoteFileExists($url));
        return $url;
    }
    
//     protected function remoteFileExists( $url ) {
//         $header_response = get_headers($url, 1);
//         return strpos( $header_response[0], "404" ) === false;
//     }
}