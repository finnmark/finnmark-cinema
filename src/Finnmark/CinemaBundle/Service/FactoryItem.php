<?php
namespace Finnmark\CinemaBundle\Service;

use Symfony\Component\HttpKernel\Log\LoggerInterface;
class FactoryItem {
    protected $_context;
    
    /**
     * 
     * @var LoggerInterface
     */
    protected $_log;
    
    /**
     * 
     * @return \Symfony\Component\HttpKernel\Log\LoggerInterface
     */
    public function getLog(){
        return $this->_log;
    }

    public function __construct( LoggerInterface $log ) {
        $this->_log = $log;
        $this->getLog()->debug('Creating FactoryItem ' . get_called_class());
    }
    
    public function setContext( $context ) {
        $this->_context = $context;
    }
    
}