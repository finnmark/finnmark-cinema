<?php
namespace Finnmark\CinemaBundle\Service\ConsistencyCheck;

use Doctrine\ORM\EntityManager;

class ConsistencyCheck {
    /**
     *
     * @var EntityManager
     */
    protected $_em;
    
    public function __construct( EntityManager $em ) {
        $this->_em = $em;
    }
    
    /**
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getManager() {
        return $this->_em;
    }
    
    /**
     * Test if seance and seat may be connected (belong to the same room/cinema).
     * 
     * @param \Finnmark\CinemaBundle\Entity\Seance $seance
     * @param \Finnmark\CinemaBundle\Entity\Seat $seat
     * 
     * @return bool
     */
    public function checkSeanceSeat( \Finnmark\CinemaBundle\Entity\Seance $seance, \Finnmark\CinemaBundle\Entity\Seat $seat ) {
        return $seance->getRoom()->getId() == $seat->getRoomArea()->getRoom()->getId();
    }
    
    /**
     * Test if reservation and booing are for the same seance.
     * 
     * @param \Finnmark\CinemaBundle\Entity\Reservation $reservation
     * @param \Finnmark\CinemaBundle\Entity\Booking $booking
     * @return boolean
     */
    public function checkReservationBooking( \Finnmark\CinemaBundle\Entity\Reservation $reservation, \Finnmark\CinemaBundle\Entity\Booking $booking ) {
        return $reservation->getSeance()->getId() == $booking->getSeance()->getId();
    }
}