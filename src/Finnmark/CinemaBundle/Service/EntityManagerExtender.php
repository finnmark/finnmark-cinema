<?php
namespace Finnmark\CinemaBundle\Service;

use Doctrine\ORM\EntityManager;
class EntityManagerExtender {
    public function __construct( EntityManager $em ) {
        $em->getConfiguration()->addCustomDatetimeFunction('DATE', '\Finnmark\CinemaBundle\Doctrine\DateFunction');
    }
}