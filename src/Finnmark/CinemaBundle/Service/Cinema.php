<?php
namespace Finnmark\CinemaBundle\Service;


use Doctrine\ORM\EntityManager;
use Finnmark\CinemaBundle\Entity\Movie;
use Finnmark;

class Cinema{
    /**
     * 
     * @var \Doctrine\ORM\EntityManager
     */
    protected $_em;
    
    public function __construct( EntityManager $em ) {
        $this->_em = $em;
    }
    
    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public function getManager() {
        return $this->_em;
    }
    
    /**
     * 
     * @return \Finnmark\CinemaBundle\Entity\Cinema
     */
    public function getCinema( $id ) {
        if( is_numeric( $id ) )  {
            return $this->getManager()->getRepository('FinnmarkCinemaBundle:Cinema')->find( $id );
        }
        else {
            return $this->getManager()->getRepository('FinnmarkCinemaBundle:Cinema')->findOneBy(array('slug'=>$id));
        }
    }
    
//     /**
//      * Get list of movies played in this cinema on given date.
//      * @param \DateTime $date
//      */
//     public function getMovies( $date ) {
//         $this->getManager()->getConfiguration()->addCustomDatetimeFunction('DATE', '\Finnmark\CinemaBundle\Doctrine\DateFunction');
        
//         $qb = $this->getManager()->createQueryBuilder();
//         $qb->select('m')
//             ->from('FinnmarkCinemaBundle:Movie', 'm')
//             ->join('m.movieVersions', 'mv')
//             ->join('mv.seances', 's')
//             ->where('DATE(s.at)=:at')->setParameter(':at', $date)
//         ;
//         return $qb->getQuery()->getResult();
//     }
    
    public function getMoviesVersions( $date ) {
        $this->getManager()->getConfiguration()->addCustomDatetimeFunction('DATE', '\Finnmark\CinemaBundle\Doctrine\DateFunction');
        
        $qb = $this->getManager()->createQueryBuilder();
        $qb->select('mv')
            ->from('FinnmarkCinemaBundle:MovieVersion', 'mv')
            ->join('mv.seances', 's')
            ->where('DATE(s.at)=:at')->setParameter(':at', $date)
        ;
        return $qb->getQuery()->getResult();
    }
}