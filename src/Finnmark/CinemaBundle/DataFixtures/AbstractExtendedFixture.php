<?php
namespace Finnmark\CinemaBundle\DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Contains some helper functions
 * @author Michał Kocztorz
 */
abstract class AbstractExtendedFixture extends AbstractFixture implements ContainerAwareInterface {
    

    private $container;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function fromArray( ObjectManager $manager, $array, $entityName ) {
        foreach( $array as $key => $entityData ) {
            $entity = new $entityName();
            foreach( $entityData as $field => $val ) {
                //first try to set values directly
                $setter = 'set' . ucfirst($field);
                if( method_exists($entity, $setter) ) {
                    $entity->$setter( $val );
                }
                //then if setter not found, try if the value is a reference
                else if( $this->hasReference( $val ) ) {
                    $ref = $this->getReference( $val );
                    $setter = 'set' . str_replace('ref:', '', $field); //convert ref:<field> to set<field>
                    if( method_exists($entity, $setter) ) {
                        $entity->$setter( $ref );
                    }
                }
                else {
                    //error
                    throw new \Exception('Unknown key: ' . $field);
                }
            }
            $manager->persist( $entity );
            $this->setReference($key, $entity);
        }
    }
    
    public function generateRandomString($length = 10) {
        $characters = '0123 456789 abcdef ghijkl mnopqrs tuvwxyz ABCDEF GHIJKLM NOPQRS TUVWXYZ.';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
    
    public function resetAutoIncrement( $table, $set=1 ) {
        $connection = $this->container->get('doctrine')->getEntityManager()->getConnection();
        $connection->exec("ALTER TABLE " . $table . " AUTO_INCREMENT = " . $set . ";");
    }
    
}