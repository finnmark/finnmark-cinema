<?php
namespace Finnmark\CinemaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finnmark\CinemaBundle\Entity\Seance;
use Finnmark\CinemaBundle\DataFixtures\AbstractExtendedFixture;

class SeanceFixtures extends AbstractExtendedFixture implements OrderedFixtureInterface {
    
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
	 */
	public function load(ObjectManager $manager) {
	    
	    $this->resetAutoIncrement('seance');
        
        $array = array(
            'seans-1' => array('ref:MovieVersion' => 'mv-1-1', 'ref:Room' => 'room-1-1', 'at' => new \DateTime('2013-10-10 12:00')),
            'seans-2' => array('ref:MovieVersion' => 'mv-1-1', 'ref:Room' => 'room-1-2', 'at' => new \DateTime('2013-10-12 22:00')),
            'seans-3' => array('ref:MovieVersion' => 'mv-2-1', 'ref:Room' => 'room-1-3', 'at' => new \DateTime('2013-10-09 09:00')),
            'seans-4' => array('ref:MovieVersion' => 'mv-3-1', 'ref:Room' => 'room-1-1', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-5' => array('ref:MovieVersion' => 'mv-4-1', 'ref:Room' => 'room-1-2', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-6' => array('ref:MovieVersion' => 'mv-5-1', 'ref:Room' => 'room-1-3', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-7' => array('ref:MovieVersion' => 'mv-6-1', 'ref:Room' => 'room-1-1', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-8' => array('ref:MovieVersion' => 'mv-7-1', 'ref:Room' => 'room-1-2', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-9' => array('ref:MovieVersion' => 'mv-8-1', 'ref:Room' => 'room-1-3', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-10' => array('ref:MovieVersion' => 'mv-9-1', 'ref:Room' => 'room-1-1', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-11' => array('ref:MovieVersion' => 'mv-10-1', 'ref:Room' => 'room-1-2', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-12' => array('ref:MovieVersion' => 'mv-3-2', 'ref:Room' => 'room-1-3', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-13' => array('ref:MovieVersion' => 'mv-3-3', 'ref:Room' => 'room-1-1', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-14' => array('ref:MovieVersion' => 'mv-2-1', 'ref:Room' => 'room-1-2', 'at' => new \DateTime('2013-10-09 16:00')),
            'seans-15' => array('ref:MovieVersion' => 'mv-2-1', 'ref:Room' => 'room-1-3', 'at' => new \DateTime('2013-10-09 16:00')),
        );

        $this->fromArray($manager, $array, '\Finnmark\CinemaBundle\Entity\Seance');
        $manager->flush();
	}

	
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
	 */
	public function getOrder() {
		return 40;

	}

}