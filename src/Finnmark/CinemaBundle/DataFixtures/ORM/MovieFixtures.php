<?php
namespace Finnmark\CinemaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finnmark\CinemaBundle\Entity\Movie;
use Finnmark\CinemaBundle\DataFixtures\AbstractExtendedFixture;

class MovieFixtures extends AbstractExtendedFixture implements OrderedFixtureInterface {
    
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
	 */
	public function load(ObjectManager $manager) {
	    $this->resetAutoIncrement('movie');
	    $movies = array(
	       'movie-1'   => array('Title'=>'Szklanką po łapkach', 'Extra'=>json_encode(array())),
	       'movie-2'   => array('Title'=>'Die hard 18', 'Extra'=>json_encode(array())),
	       'movie-3'   => array('Title'=>'Skyfall', 'Extra'=>json_encode(array())),
	       'movie-4'   => array('Title'=>'Gra endera', 'Extra'=>json_encode(array())),
	       'movie-5'   => array('Title'=>'Atlas chmur', 'Extra'=>json_encode(array())),
	       'movie-6'   => array('Title'=>'The Help', 'Extra'=>json_encode(array())),
	       'movie-7'   => array('Title'=>'Casino Royal', 'Extra'=>json_encode(array())),
	       'movie-8'   => array('Title'=>'Quantum of solace', 'Extra'=>json_encode(array())),
	       'movie-9'   => array('Title'=>'Pulp fiction', 'Extra'=>json_encode(array())),
	       'movie-10'   => array('Title'=>'Nieśmiertelny', 'Extra'=>json_encode(array())),
	    );
	    
	    $this->fromArray($manager, $movies, "\Finnmark\CinemaBundle\Entity\Movie");
        
        $manager->flush();
	}
    
	
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
	 */
	public function getOrder() {
        return 1;
	}

}