<?php
namespace Finnmark\CinemaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finnmark\CinemaBundle\Entity\Seat;
use Finnmark\CinemaBundle\DataFixtures\AbstractExtendedFixture;
class SeatFixtures extends AbstractExtendedFixture implements OrderedFixtureInterface {
    
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
	 */
	public function load(ObjectManager $manager) {
	    
	    $this->resetAutoIncrement('seat');
	    
        $roomArea1 = $this->getReference('ra-1-1-left');
        $roomArea2 = $this->getReference('ra-1-1-right');
        
        $rows = 12;
        $cols = 6;
        $width = 20;
        $height = 20;
        $marginL = 5;
        $marginH = 5;
        
        $i = 1;
        //area 1
        for( $r=0; $r< $rows; $r++ ) {
            for( $c=0; $c<$cols; $c++ ){
                $seat = new Seat();
                $seat->setRoomArea( $roomArea1 );
                $name = $i++;
                $seat->setName($name);
                $map = array(
                    'left' => $c * ($width+$marginL),
                    'top' => $r * ($height+$marginH),
                    'width' => $width,
                    'height' => $height
                );
                $map = json_encode($map);
                $seat->setMap($map);
                $manager->persist($seat);
                
                $this->setReference($name, $seat);
            }
        }
        
        //area 2
        for( $r=0; $r< $rows; $r++ ) {
            for( $c=0; $c<$cols; $c++ ){
                $seat = new Seat();
                $seat->setRoomArea( $roomArea2 );
                $name = $i++;
                $seat->setName($name);
                $map = array(
                    'left' => $c * ($width+$marginL),
                    'top' => $r * ($height+$marginH),
                    'width' => $width,
                    'height' => $height
                );
                $map = json_encode($map);
                $seat->setMap($map);
                $manager->persist($seat);
                
                $this->setReference($name, $seat);
            }
        }
        
        $manager->flush();
        
	}
	
	
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
	 */
	public function getOrder() {
        return 30;
	}


}