<?php
namespace Finnmark\CinemaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finnmark\CinemaBundle\Entity\MovieVersion;
use Finnmark\CinemaBundle\DataFixtures\AbstractExtendedFixture;

class MovieVersionFixtures extends AbstractExtendedFixture implements OrderedFixtureInterface {
    
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
	 */
    public function load(ObjectManager $manager) {
        $this->resetAutoIncrement('movie_version');
        $versions = array(
            'mv-1-1'    => array('ref:movie'=>'movie-1', 'Length'=>89, 'Type'=>'2D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-2-1'    => array('ref:movie'=>'movie-2', 'Length'=>112, 'Type'=>'2D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-3-1'    => array('ref:movie'=>'movie-3', 'Length'=>145, 'Type'=>'2D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-3-2'    => array('ref:movie'=>'movie-3', 'Length'=>200, 'Type'=>'3D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-3-3'    => array('ref:movie'=>'movie-3', 'Length'=>90, 'Type'=>'8D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-4-1'    => array('ref:movie'=>'movie-4', 'Length'=>100, 'Type'=>'2D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-5-1'    => array('ref:movie'=>'movie-5', 'Length'=>101, 'Type'=>'2D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-6-1'    => array('ref:movie'=>'movie-6', 'Length'=>98, 'Type'=>'2D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-7-1'    => array('ref:movie'=>'movie-7', 'Length'=>99, 'Type'=>'2D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-8-1'    => array('ref:movie'=>'movie-8', 'Length'=>103, 'Type'=>'2D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-9-1'    => array('ref:movie'=>'movie-9', 'Length'=>150, 'Type'=>'2D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
            'mv-10-1'   => array('ref:movie'=>'movie-10', 'Length'=>134, 'Type'=>'3D', 'Description'=>$this->generateRandomString(rand(200, 500)), ),
	    );
	    
	    $this->fromArray($manager, $versions, '\Finnmark\CinemaBundle\Entity\MovieVersion');

	    $manager->flush();
	}
    
	
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
	 */
	public function getOrder() {
        return 10;
	}

}