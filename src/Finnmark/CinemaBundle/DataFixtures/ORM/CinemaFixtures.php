<?php
namespace Finnmark\CinemaBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Finnmark\CinemaBundle\Entity\Cinema;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Finnmark\CinemaBundle\DataFixtures\AbstractExtendedFixture;

class CinemaFixtures extends AbstractExtendedFixture implements OrderedFixtureInterface {
    
    public function load(ObjectManager $manager) {
        /*
         * Cinema ID is part of the configuration of the app.
         * Since the configuration doesn't change on loading fixtures I need to make sure
         * that generated cinemas get the same IDs.
         * 
         * Other fixtures don't require this.
         */
        $this->resetAutoIncrement('cinema');
        
        $cinema1 = new Cinema();
        $cinema1->setName('Sample Cinema 1');
        $manager->persist($cinema1);
        
        $manager->flush();
        
        $this->addReference('cinema-1', $cinema1);
    }
    
    public function getOrder() {
        return 1;
    }
}