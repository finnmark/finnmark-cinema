<?php
namespace Finnmark\CinemaBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finnmark\CinemaBundle\Entity\RoomArea;
use Finnmark\CinemaBundle\DataFixtures\AbstractExtendedFixture;

class RoomAreaFixtures extends AbstractExtendedFixture implements OrderedFixtureInterface {
    
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
	 */
	public function load(ObjectManager $manager) {
	    $mapLeft = json_encode(array(
            'width' => 150,
            'height' => 300,
            'left' => 0,
            'top' => 0
        ));
	    $mapRight = json_encode(array(
            'width' => 150,
            'height' => 300,
            'left' => 200,
            'top' => 0
        ));
	    
	    $array = array(
	       'ra-1-1-left'   => array('ref:Room'=>'room-1-1', 'Name'=>'Room Area 1.1 Left', 'Map'=>$mapLeft),
	       'ra-1-1-right'  => array('ref:Room'=>'room-1-1', 'Name'=>'Room Area 1.1 Right', 'Map'=>$mapRight),
	       'ra-1-2-left'   => array('ref:Room'=>'room-1-2', 'Name'=>'Room Area 1.2 Left', 'Map'=>$mapLeft),
	       'ra-1-2-right'  => array('ref:Room'=>'room-1-2', 'Name'=>'Room Area 1.2 Right', 'Map'=>$mapRight),
	       'ra-1-3-left'   => array('ref:Room'=>'room-1-3', 'Name'=>'Room Area 1.3 Left', 'Map'=>$mapLeft),
	       'ra-1-3-right'  => array('ref:Room'=>'room-1-3', 'Name'=>'Room Area 1.3 Right', 'Map'=>$mapRight),
	    );
	    
	    $this->fromArray($manager, $array, '\Finnmark\CinemaBundle\Entity\RoomArea');
        
        $manager->flush();
	}
	
	
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
	 */
	public function getOrder() {
	    return 20;
	}


}