<?php
namespace Finnmark\CinemaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finnmark\CinemaBundle\Entity\Booking;
use Finnmark\CinemaBundle\DataFixtures\AbstractExtendedFixture;

class BookingFixtures extends AbstractExtendedFixture implements OrderedFixtureInterface {
    
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
	 */
	public function load(ObjectManager $manager) {
	    $this->resetAutoIncrement('booking');

	    $array = array(
	       'book1' => array('Status'=>0, 'CreatedAt'=>new \DateTime('2013-10-10 12:12:12'), 'ref:Seance'=>'seans-1'),
	    );
	    
	    $this->fromArray($manager, $array, '\Finnmark\CinemaBundle\Entity\Booking');
        
//         $bookings = array(
//             array('ref'=>'book1', 'status'=>0, 'crt'=> new \DateTime('2013-10-10 12:12:12')),
//             array('ref'=>'book2', 'status'=>0, 'crt'=> new \DateTime('2013-10-11 02:12:12')),
//             array('ref'=>'book3', 'status'=>0, 'crt'=> new \DateTime('2013-10-11 22:12:12')),
//             array('ref'=>'book4', 'status'=>0, 'crt'=> new \DateTime('2013-10-11 14:18:12')),
//             array('ref'=>'book5', 'status'=>0, 'crt'=> new \DateTime('2013-10-12 10:00:00')),
//         );
        
//         foreach( $bookings as $book ) {
//             $booking = new Booking();
//             $booking->setStatus($book['status']);
//             $booking->setCreatedAt($book['crt']);
//             $manager->persist($booking);
//             $this->setReference($book['ref'], $booking);
//         }
        
        $manager->flush();
	}

	
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
	 */
	public function getOrder() {
        return 50;
	}

}