<?php
namespace Finnmark\CinemaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Finnmark\CinemaBundle\Entity\Room;
use Finnmark\CinemaBundle\DataFixtures\AbstractExtendedFixture;

class RoomFixtures extends AbstractExtendedFixture implements OrderedFixtureInterface {

    /* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
	 */
	public function load(ObjectManager $manager) {
	    $map = json_encode(array(
            'width' => 350,
            'height' => 300,
        ));
	    
        $rooms = array(
            'room-1-1'  => array('ref:cinema'=>'cinema-1', 'Name'=>'Room 1.1', 'Map'=>$map),
            'room-1-2'  => array('ref:cinema'=>'cinema-1', 'Name'=>'Room 1.2', 'Map'=>$map),
            'room-1-3'  => array('ref:cinema'=>'cinema-1', 'Name'=>'Room 1.3', 'Map'=>$map),
        );
        
        $this->fromArray($manager, $rooms, '\Finnmark\CinemaBundle\Entity\Room');
        $manager->flush();
	}
	
	
	/* (non-PHPdoc)
	 * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
	 */
	public function getOrder() {
	    return 10;
	}


}