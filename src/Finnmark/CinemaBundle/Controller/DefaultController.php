<?php

namespace Finnmark\CinemaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Finnmark\CinemaBundle\Service\Cinema;
use Finnmark\CinemaBundle\Service\Repertoire;
use Finnmark\CinemaBundle\Exception\AbstractException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Template()
     */
    public function debugAction() {
        /*
         * Booking
         */
        $data = array();
        /* @var $booking \Finnmark\CinemaBundle\Service\Booking\Booking */
        $bookingService = $this->get('finnmark_cinema.booking');
        $em = $this->getDoctrine()->getManager();
        try {
            $bookingVault = $this->get('finnmark_cinema.booking_vault');
            $seance     = $em->getRepository('\Finnmark\CinemaBundle\Entity\Seance')->find( 1 );
            $booking    = $bookingVault->getBooking( $seance );
            
            
            $seat       = $em->getRepository('\Finnmark\CinemaBundle\Entity\Seat')->find( 1 );
            $reservation = $bookingService->book($seance, $seat, $booking);
            $em->persist( $booking );
            $em->persist( $reservation );
            $em->flush();
        }
        catch( AbstractException $ae ) {
            //booking failed
            $data['error'] = $ae->getMessage();
        }
        catch( \Exception $e ) {
            //other unexpected exception
            $data['error'] = $e->getMessage();
        }
        return $data;
    }
    
    /**
     * The cinema service is preloaded with configured cinema entity.
     * 
     * @return Cinema
     */
    protected function _getCinemaService() {
        return $this->get('finnmark_cinema.cinema');
    }
    
    /**
     * 
     * @return Repertoire
     */
    protected function _getRepertoireService() {
        return $this->get('finnmark_cinema.repertoire');
    }
    
    /**
     * @Template()
     */
    public function indexAction()
    {
        $cinema = $this->_getCinema();
        $em = $this->getDoctrine()->getManager();
        $rooms = $em->getRepository('FinnmarkCinemaBundle:Room')->getAll( $cinema->getCinema() );
        return array('rooms'=>$rooms);
    }
    
    /**
     * @Template()
     */
    public function moviesAction() {
        $cinema = $this->_getCinema();
//         $movies = $cinema->getCurrentMovies();
        
        $movies = $cinema->getMovies( new \DateTime('2013-10-09') );
        var_dump(count($movies[0]->getMovieVersions()));
        return array('movies'=>$movies);
    }
    
    /**
     * 
     * @Template()
     */
    public function listCinemasAction() {
        $factory = $this->get('finnmark_cinema.di.bucket');
        $a = new \stdClass();
        $b = array(new \stdClass(), $this);
        $factory->get('finnmark_cinema.factory.item', $b );
        $factory->get('finnmark_cinema.factory.item', $b);
        $factory->get('finnmark_cinema.factory.item', $a);
        return array();
    }

    /**
     * 
     * @Template()
     */
    public function repertoireAction( $cinemaSlug, $date ) {
        $media = $this->get('finnmark_cinema.movie_media');
        $repertoire = $this->_getRepertoireService();
        $moviesVersions = $repertoire->getByDay( $cinemaSlug, $date );
        return array('moviesVersions'=>$moviesVersions, 'media'=>$media);
    }
    
    /**
     * @Template()
     */
    public function seanceBookingAction( $seance ) {
        $bookingViewDataProvider = $this->get('finnmark_cinema.booking_view');
        $data = $bookingViewDataProvider->prepareData( $seance );
        return $data;
    }

    /**
     * @Route("/booking", name="ajax-booking-room")
     * @Template()
     */
    public function bookingAjaxAction() {
        $bookingViewDataProvider = $this->get('finnmark_cinema.booking_view');
        $data = $bookingViewDataProvider->prepareData( $this->getRequest()->get('seance', 0) );
        $data['ajaxResponse'] = true;
        return $data;
    }
    
    /**
     * 
     * @Route("/booking/save", name="ajax-booking-save")
     */
    public function saveBookingAction() {
        $selection = $this->getRequest()->get('selection');
        $seance = $this->getRequest()->get('seance');
        $rc = $this->get( 'finnmark_cinema.reservations_creator' );
        if( $rc->makeReservations($seance, $selection) ) {
            $ret = true;
        }
        else {
            $ret = $this->get('translator')->trans('Your selection is not valid. Possibly someone else booked the same seats in the same time you did. Select different seats please.');
        }
        return new Response(json_encode($ret));
    }
    
    /**
     * Confirmation view for saved booking.
     * @Route("/booking/confirm", name="ajax-booking-confirm")
     * @Template()
     */
    public function confirmationAjaxAction() {
        $bookingConfirmationViewDataProvider = $this->get('finnmark_cinema.booking_view');
        $data = $bookingConfirmationViewDataProvider->prepareData( $this->getRequest()->get('seance', 0) );
        $data['ajaxResponse'] = true;
        return $data;
    }
}
