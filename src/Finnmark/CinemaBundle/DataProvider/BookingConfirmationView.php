<?php
namespace Finnmark\CinemaBundle\DataProvider;

use Finnmark\CinemaBundle\Service\Entity\Loader;
use Finnmark\CinemaBundle\Service\Booking\Vault;
/**
 * Helper service to provide data for booking/room view
 */
class BookingConfirmationView {
    /**
     *
     * @var Loader
     */
    protected $_loader;
    
    /**
     *
     * @var Vault
     */
    protected $_vault;
    
    public function __construct( Loader $loader, Vault $vault) {
        $this->_loader = $loader;
        $this->_vault = $vault;
    }
    
    /**
     * Prepares data for booking view.
     * @param string $seance
     * @return array
     */
    public function prepareData( $seance ) {
        $data = array();
        try{
            $seanceEntity  = $this->_loader->findSeance( $seance );
            $bookingEntity = $this->_vault->getBooking( $seanceEntity );
            $data['seance'] = $seanceEntity;
            $data['booking'] = $bookingEntity;
            $data['reservations'] = $bookingEntity->getReservations();
        }
        catch( \Exception $e ) {
            $data[ 'error' ] = $e->getMessage();
        }
        return $data;
    }
}