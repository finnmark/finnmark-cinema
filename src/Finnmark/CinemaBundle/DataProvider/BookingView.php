<?php
namespace Finnmark\CinemaBundle\DataProvider;

use Finnmark\CinemaBundle\Service\Entity\Loader;
use Finnmark\CinemaBundle\Service\Booking\Vault;
use Finnmark\CinemaBundle\Service\Booking\Booking;
/**
 * Helper service to provide data for booking/room view
 */
class BookingView {
    /**
     *
     * @var Loader
     */
    protected $_loader;
    
    /**
     *
     * @var Vault
     */
    protected $_vault;
    
    /**
     *
     * @var Booking
     */
    protected $_booking;
    
    public function __construct( Loader $loader, Vault $vault, Booking $booking ) {
        $this->_loader = $loader;
        $this->_vault = $vault;
        $this->_booking = $booking;
    }
    
    /**
     * Prepares data for booking view.
     * @param string $seance
     * @return array
     */
    public function prepareData( $seance ) {
        $data = array();
        try{
            $seanceEntity  = $this->_loader->findSeance( $seance );
            $bookingEntity = $this->_vault->getBooking( $seanceEntity );
            $data['seance'] = $seanceEntity;
            $data['booking'] = $bookingEntity;
            $data['bookingService'] = $this->_booking;
        }
        catch( \Exception $e ) {
//            var_dump($e->getMessage());
            $data[ 'error' ] = $e->getMessage();
        }
        return $data;
    }
}