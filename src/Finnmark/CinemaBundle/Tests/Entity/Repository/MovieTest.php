<?php
namespace Finnmark\CinemaBundle\Tests\Entity\Repository;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\ORM\EntityManager;
use Finnmark\CinemaBundle\Entity\Repository\MovieRepository;

class MovieTest extends WebTestCase{
    
    /**
     * 
     * @var EntityManager
     */
    protected $_em;
    
    /**
     * 
     * @var MovieRepository
     */
    protected $_movieRepository;
    
    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->_em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->_movieRepository = $this->_em->getRepository('FinnmarkCinemaBundle:Movie');
    }
    
    
    public function testGetCurrent() {
        $current = $this->_movieRepository->getCurrent();
        $this->assertTrue(is_array($current));
        $this->assertTrue(count($current)>0);
    }
}