<?php
namespace Finnmark\CinemaBundle\DependencyInjection\Context;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;


class Bucket {
    
    /**
     * 
     * @var ContainerInterface
     */
    protected $_container;
    
    /**
     * 
     * @var LoggerInterface
     */
    protected $_log;
    
    /**
     * 
     * @return \Symfony\Component\HttpKernel\Log\LoggerInterface
     */
    public function getLog(){
        return $this->_log;
    }
    
    /**
     * 
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected function _getContainer() {
        return $this->_container;
    }
    
    protected $_services = array();
    
    public function __construct( ContainerInterface $container,  LoggerInterface $log  ) {
        $this->_container = $container;
        $this->_log = $log;
        $this->getLog()->debug("Creating factory " . get_called_class());
    }
    
    /**
     * Get service for given dynamic context.
     * 
     * @param string $service_id
     * @param mixed $context
     * 
     * @return \stdClass
     */
    public function get( $service_id, $context ) {
        $this->_prepareServiceStorage($service_id);
        $contextString = $this->_context2string( $context );
        if( !isset( $this->_services[ $service_id ][ $contextString ]) ) {
            $srv = $this->_getContainer()->get( $service_id );
            if( method_exists($srv, 'setKey' ) ) { //make it interface
                $srv->setKey( $context );
            }
            $this->_services[ $service_id ][ $contextString ] = $srv;
        }
//         var_dump($contextString);
        return $this->_services[ $service_id ][ $contextString ];
    }
    
    /**
     * Prepare first level of storage for created services.
     * @param string $service_id
     */
    protected function _prepareServiceStorage( $service_id ) {
        if( !isset( $this->_services[ $service_id ] ) ) {
            $this->_services[ $service_id ] = array();
        }
    }
    
    /**
     * Produce a unique string handle for passed key.
     * @param mixed $context
     * @return string
     */
    protected function _context2string( $context ) {
        if( is_string($context) ) {
            return 'str_' . $context;
        }
        
        if( is_object($context) ) {
            return 'obj_' . md5( spl_object_hash( $context ) );
        }
        
        if( is_array( $context ) ) {
            //array of objects? to avoid serializing
            $onlyObjects = count($context) > 0;
            foreach( $context as $object ) {
                $onlyObjects = $onlyObjects && is_object( $object );
            }
            if( $onlyObjects ) {
                $ret = '';
                foreach( $context as $object ) {
                    $ret .= spl_object_hash( $object );
                }
                return 'aoo_' . md5( $ret ); //aoo = array of objects
            }
            
            return 'arr_' . md5(serialize($context));
        }
        return (string) 'other_' . $context;
    }
}