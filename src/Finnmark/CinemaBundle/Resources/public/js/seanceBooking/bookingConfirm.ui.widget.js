/**
 * The confirmation step.
 * 
 */
(function($){
    $.widget( "finnmark.bookingConfirmUiWidget", $.finnmark.baseUiWidget, {
        _create: function() {
            this.options.classes = {
            };
            $.finnmark.baseUiWidget.prototype._create.call(this);
        },
        
        attachEventHandlers: function() {
        }
    });
})(jQuery);