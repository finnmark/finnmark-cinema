(function($){
    $.widget( "finnmark.bookingUiWidget", $.finnmark.baseUiWidget, {
        _create: function() {
            this.options.classes = {
                seat: 'room-seat',
                seat_taken: 'room-seat-taken',
                seat_my: 'room-seat-my'
            };
            $.finnmark.baseUiWidget.prototype._create.call(this);
            this.seats = this.element.find('.'+this.options.classes.seat);
            this.seats.click(this.clickSeat.bind(this));
            this.reservationList = this.element.find('.reservations-list');
            this.attachEventHandlers();
            this.rebuildReservationList();
            this.element.show();
        },
        
        attachEventHandlers: function() {
            this.element.on('seatToggle', function(event, seat){ this.seatToggle(seat); }.bind(this));
            this.element.on('seatBooked', function(event, seat){ this.seatBooked(seat); }.bind(this));
            this.element.on('seatReleased', function(event, seat){ this.seatReleased(seat); }.bind(this));
        },
        
        /**
         * Physical event: seat was clicked
         */
        clickSeat: function( event ) {
            this.debug(event);
            this.element.trigger('seatToggle', [$(event.delegateTarget)]);
        },
        
        /**
         * A seat state changed.
         * @param seat $(DOM) <div.room-seat>
         */
        seatToggle: function( seat ) {
            this.debug( seat );
            if( !this.isSeatTaken( seat ) ) {
                seat.toggleClass(this.options.classes.seat_my);
                if( this.isSeatMy( seat ) ) {
                    this.element.trigger('seatBooked', [seat]);
                }
                else {
                    this.element.trigger('seatReleased', [seat]);
                }
            }
        },
        
        /**
         * A seat was booked.
         * @param seat $(DOM) <div.room-seat>
         */
        seatBooked: function( seat ) {
            this.debug('booked');
            this.rebuildReservationList();
        },
        
        /**
         * A seat was released.
         * @param seat $(DOM) <div.room-seat>
         */
        seatReleased: function( seat ) {
            this.debug('released');
            this.rebuildReservationList();
        },
        
        /**
         * Rebuild the list of reserved seats.
         */
        rebuildReservationList: function() {
            this.reservationList.empty();
            var selected = this.element.find( '.' + this.options.classes.seat_my );
            this.element.data( 'selection', [] );
            $.each( selected, function( index, seat ) {
                this.addSeatToReservationsList( $(seat) );
            }.bind(this));
        },
        
        /**
         * Test if seat is my.
         * @param seat $(DOM) <div.room-seat>
         */
        addSeatToReservationsList: function( seat ) {
            var selection = this.element.data('selection');
            selection.push(seat.data('id'));
            this.element.data( 'selection', selection );
            this.reservationList.append( $('<li class="list-group-item">' + seat.data('name') + '</li>') );
        },
        
        /**
         * Test if seat is my.
         * @param seat $(DOM) <div.room-seat>
         */
        isSeatMy: function( seat ) {
            return seat.hasClass( this.options.classes.seat_my );
        },
        
        /**
         * Test if seat is taken.
         * @param seat $(DOM) <div.room-seat>
         */
        isSeatTaken: function( seat ) {
            return seat.hasClass( this.options.classes.seat_taken );
        }
    });
})(jQuery);