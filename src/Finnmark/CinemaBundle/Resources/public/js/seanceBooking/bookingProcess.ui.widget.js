(function($){
    /*
     * This widget is supervising the process of making reservation.
     */
    $.widget( "finnmark.bookingProcessUiWidget", $.finnmark.baseUiWidget, {
        _create: function() {
            this.options.classes = {
                body: 'booking-process-body',
                busy: 'booking-process-busy',
                errors: 'booking-process-errors',
                errorMessage: 'error-message',
                step: 'booking-process-step',
                roomClass: 'booking',
                confirmClass: 'confirm',
                stepRoomClass: 'booking-process-step-room',
                stepConfirmClass: 'booking-process-step-confirm',
                btnPrev: 'step-prev',
                btnNext: 'step-next'
            };
            this.options.errorTemplate = '<div class="' + this.options.classes.errorMessage  + '"></div>';
            $.finnmark.baseUiWidget.prototype._create.call(this);
            
            this.step = 'none'; //waiting to initialize, or changing step
            
            this.body = this.element.find('.' + this.options.classes.body);
            this.busy = this.element.find('.' + this.options.classes.busy);
            this.stepContainers = this.element.find('.' + this.options.classes.step);
            
            this.stepRoom = this.element.find('.' + this.options.classes.stepRoomClass);
            this.stepConfirm = this.element.find('.' + this.options.classes.stepConfirmClass);
            
            this.btnPrev = this.element.find('.' + this.options.classes.btnPrev);
            this.btnNext = this.element.find('.' + this.options.classes.btnNext);
            this.errors = this.element.find('.' + this.options.classes.errors);
            
            this.attachEventHandlers();
            /*
             * If the room view is not loaded, do it.
             */
            if( this.element.find( '.' + this.options.classes.roomClass ).length === 0 ) {
                /*
                 * The appropriate html is not loaded, so load it with ajax. 
                 * The ajax call sill initRoom() on success.
                 */
                this.loadRoom( this.options.seance );
            }
            else {
                this.initRoom();
            }
        },
        
        attachEventHandlers: function() {
            this.element.on('stepChanged', this.initStep.bind(this));
        },
/*
 * Error display handling
 */
        /**
         * Gets data value from errors DOM element and passes it to displayError.
         * @param string key
         * @returns null
         */
        displayDataError: function( key ) {
            this.displayError( this.errors.data(key) );
        },
        
        displayError: function( message ) {
            var tmpl = $(this.options.errorTemplate);
            var inner = tmpl;
            if( !tmpl.hasClass(this.options.classes.errorMessage) ) {
                inner = tmpl.find('.' + this.options.classes.errorMessage);
            }
            this.errors.append(inner.html(message));
            this.errors.show();
        },
        
        clearErrors: function() {
            this.errors.hide();
            this.errors.empty();
        },
        
/*
 * Look & Feel
 */
        busyOn: function() {
            this.body.hide();
            this.busy.show();
        },
        
        busyOff: function() {
            this.busy.hide();
            this.body.show();
        },
        
        /**
         * Initialises current step (this.step)
         * @returns null
         */
        initStep: function() {
            this.btnNextDisable();
            this.btnPrevDisable();
            switch( this.step ) {
                case 'room':
                    this.btnNextConfirm();
                    break;
                case 'confirm':
                    this.stepRoom.empty();
                    this.btnNextDone();
                    this.btnPrevRoom();
                    break;
                case 'none':
                default:
                    break;
            }
        },
        
/*
 * Prev/Next buttons related functions. Adding/Removing event handlers.
 */
        btnNextDisable: function() {
            this.btnNext.addClass('disabled').unbind('click');
        },
        
        btnNextConfirm: function() {
            this.btnNext.removeClass('disabled');
            this.btnNext.on('click', this.stepRoomProceed.bind(this));
        },
        
        btnNextDone: function() {
            
        },
        
        btnPrevDisable: function() {
            this.btnPrev.addClass('disabled').unbind('click');
        },
        
        btnPrevRoom: function() {
            
        },
        
        /**
         * Ajax load room view.
         * @param int seance
         * @returns null
         */
        loadRoom: function( seance ) {
            this.busyOn();
            this.stepRoom.empty();
            $.ajax({
                url: this.options.roomUrl,
                dataType: 'html',
                context: this,
                type: 'POST',
                data: {
                    seance: seance
                }
            }).done(function( html ){
                this.stepRoom.html( html );
                this.initRoom();
            });
        },
        
        /**
         * Initialize booking widget (step 1)
         * @returns null
         */
        initRoom: function() {
            $( '.' + this.options.classes.roomClass ).bookingUiWidget();
            this.busyOff();
            this.step = 'room';
            this.element.trigger('stepChanged');
        },
        
        /**
         * User clicked on proceed to confirmation button.
         */
        stepRoomProceed: function() {
            this.debug('Proceeding to step confirm');
            var roomDOM = this.element.find( '.' + this.options.classes.roomClass );
            var selection = roomDOM.data('selection');
            this.clearErrors();
            if( selection.length > 0 ) {
                this.saveSelection( selection );
            }
            else {
                this.displayDataError( 'messageSelectionEmpty' );
            }
        },
        
         /**
          * 
          * Ajax: send user selection to the server
          * Response: 
          *  bool:
          *      true: selection accepted and saved
          *      false: selection accepted and saved
          * @param Array selection
          * @returns {undefined}
          */
        saveSelection: function( selection ) {
            this.debug( selection );
            this.busyOn();
            $.ajax({
                url: this.options.saveUrl,
                dataType: 'json',
                context: this,
                type: 'POST',
                data: {
                    selection: selection,
                    seance: this.options.seance 
                }
            }).done(function( result ){
                this.clearErrors();
                if( result === true ) {
                    this.loadConfirm( this.options.seance );
//                    this.busyOff();
                }
                else {
                    this.displayError( result );
                    this.loadRoom( this.options.seance );
                }
            }.bind(this));
        },
        
        loadConfirm: function( seance ) {
            this.busyOn();
            this.stepConfirm.empty();
            $.ajax({
                url: this.options.confirmUrl,
                dataType: 'html',
                context: this,
                type: 'POST',
                data: {
                    seance: seance
                }
            }).done(function( html ){
                this.stepConfirm.html( html );
                this.initConfirm();
            });
        },
        
        /**
         * Initialize confirn widget (step 2)
         * @returns null
         */
        initConfirm: function() {
            $( '.' + this.options.classes.confirmClass ).bookingConfirmUiWidget();
            this.busyOff();
            this.step = 'confirm';
            this.element.trigger('stepChanged');
        }
    });
    
    $('.booking-process').bookingProcessUiWidget();
})(jQuery);