(function($){
    $.widget( "finnmark.baseUiWidget", {
        _create: function() {
        	this.optionsDefault = {
    			log: true
        	};
        	
            this.options = $.extend({}, this.optionsDefault, this.options, this.element.data());
            this.debug(this.options);
        },

        debug: function( msg ) {
            if( this.options.log && console && console.log ){
                console.log( msg );
            }
        }
    });
})(jQuery)