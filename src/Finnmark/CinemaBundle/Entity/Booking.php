<?php

namespace Finnmark\CinemaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Booking
 *
 * @ORM\Table(name="booking", indexes={@ORM\Index(name="fk_booking_seance1_idx", columns={"seance_id"})})
 * @ORM\Entity
 */
class Booking
{
    const STATUS_OPENED = 0;
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    

    /**
     * @var \Finnmark\CinemaBundle\Entity\Seance
     *
     * @ORM\ManyToOne(targetEntity="Finnmark\CinemaBundle\Entity\Seance",inversedBy="bookings")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="seance_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $seance;
    
    /**
     * @ORM\OneToMany(targetEntity="Reservation",mappedBy="booking")
     * @var ArrayCollection
     */
    private $reservations;


    public function __construct() {
        $this->setStatus( self::STATUS_OPENED );
        $this->setCreatedAt( new \DateTime('now') );
        $this->reservations = new ArrayCollection();
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Booking
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Booking
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seance
     *
     * @param \Finnmark\CinemaBundle\Entity\Seance $seance
     * @return Booking
     */
    public function setSeance(\Finnmark\CinemaBundle\Entity\Seance $seance = null)
    {
        $this->seance = $seance;

        return $this;
    }

    /**
     * Get seance
     *
     * @return \Finnmark\CinemaBundle\Entity\Seance 
     */
    public function getSeance()
    {
        return $this->seance;
    }

    /**
     * Add reservations
     *
     * @param \Finnmark\CinemaBundle\Entity\Reservation $reservations
     * @return Booking
     */
    public function addReservation(\Finnmark\CinemaBundle\Entity\Reservation $reservations)
    {
        $this->reservations[] = $reservations;

        return $this;
    }

    /**
     * Remove reservations
     *
     * @param \Finnmark\CinemaBundle\Entity\Reservation $reservations
     */
    public function removeReservation(\Finnmark\CinemaBundle\Entity\Reservation $reservations)
    {
        $this->reservations->removeElement($reservations);
    }

    /**
     * Get reservations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservations()
    {
        return $this->reservations;
    }
}
