<?php

namespace Finnmark\CinemaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Room
 *
 * @ORM\Table(name="room", indexes={@ORM\Index(name="fk_room_cinema_idx", columns={"cinema_id"})})
 * @ORM\Entity(repositoryClass="Finnmark\CinemaBundle\Entity\Repository\RoomRepository")
 */
class Room
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="map", type="text", nullable=false)
     */
    private $map;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Finnmark\CinemaBundle\Entity\Cinema
     *
     * @ORM\ManyToOne(targetEntity="Finnmark\CinemaBundle\Entity\Cinema")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cinema_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $cinema;

    /**
     *
     * @ORM\OneToMany(targetEntity="RoomArea",mappedBy="room")
     * @var ArrayCollection
     */
    private $roomAreas;

    public function __construct() {
        $this->roomAreas = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Room
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set map
     *
     * @param string $map
     * @return Room
     */
    public function setMap($map)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return string 
     */
    public function getMap()
    {
        return json_decode($this->map);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cinema
     *
     * @param \Finnmark\CinemaBundle\Entity\Cinema $cinema
     * @return Room
     */
    public function setCinema(\Finnmark\CinemaBundle\Entity\Cinema $cinema = null)
    {
        $this->cinema = $cinema;

        return $this;
    }

    /**
     * Get cinema
     *
     * @return \Finnmark\CinemaBundle\Entity\Cinema 
     */
    public function getCinema()
    {
        return $this->cinema;
    }

    /**
     * Add roomAreas
     *
     * @param \Finnmark\CinemaBundle\Entity\RoomArea $roomAreas
     * @return Room
     */
    public function addRoomArea(\Finnmark\CinemaBundle\Entity\RoomArea $roomAreas)
    {
        $this->roomAreas[] = $roomAreas;

        return $this;
    }

    /**
     * Remove roomAreas
     *
     * @param \Finnmark\CinemaBundle\Entity\RoomArea $roomAreas
     */
    public function removeRoomArea(\Finnmark\CinemaBundle\Entity\RoomArea $roomAreas)
    {
        $this->roomAreas->removeElement($roomAreas);
    }

    /**
     * Get roomAreas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoomAreas()
    {
        return $this->roomAreas;
    }
}
