<?php

namespace Finnmark\CinemaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * RoomArea
 *
 * @ORM\Table(name="room_area", indexes={@ORM\Index(name="fk_room_area_room1_idx", columns={"room_id"})})
 * @ORM\Entity(repositoryClass="Finnmark\CinemaBundle\Entity\Repository\RoomAreaRepository")
 */
class RoomArea
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="map", type="text", nullable=false)
     */
    private $map;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Finnmark\CinemaBundle\Entity\Room
     *
     * @ORM\ManyToOne(targetEntity="Finnmark\CinemaBundle\Entity\Room", inversedBy="roomAreas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="room_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $room;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="Seat",mappedBy="roomArea")
     * @var ArrayCollection
     */
    private $seats;

    public function __construct() {
        $this->seats = new ArrayCollection();
    }


    /**
     * Set name
     *
     * @param string $name
     * @return RoomArea
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set map
     *
     * @param string $map
     * @return RoomArea
     */
    public function setMap($map)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return string 
     */
    public function getMap()
    {
        return json_decode($this->map);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set room
     *
     * @param \Finnmark\CinemaBundle\Entity\Room $room
     * @return RoomArea
     */
    public function setRoom(\Finnmark\CinemaBundle\Entity\Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \Finnmark\CinemaBundle\Entity\Room 
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Add seats
     *
     * @param \Finnmark\CinemaBundle\Entity\Seat $seats
     * @return RoomArea
     */
    public function addSeat(\Finnmark\CinemaBundle\Entity\Seat $seats)
    {
        $this->seats[] = $seats;

        return $this;
    }

    /**
     * Remove seats
     *
     * @param \Finnmark\CinemaBundle\Entity\Seat $seats
     */
    public function removeSeat(\Finnmark\CinemaBundle\Entity\Seat $seats)
    {
        $this->seats->removeElement($seats);
    }

    /**
     * Get seats
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSeats()
    {
        return $this->seats;
    }
}
