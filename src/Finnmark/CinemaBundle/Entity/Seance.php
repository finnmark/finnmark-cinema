<?php

namespace Finnmark\CinemaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Seance
 *
 * @ORM\Table(name="seance", indexes={@ORM\Index(name="fk_seance_movie_version1_idx", columns={"movie_version_id"}), @ORM\Index(name="fk_seance_room1_idx", columns={"room_id"})})
 * @ORM\Entity(repositoryClass="Finnmark\CinemaBundle\Entity\Repository\SeanceRepository")
 */
class Seance
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="at", type="datetime", nullable=false)
     */
    private $at;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Finnmark\CinemaBundle\Entity\Room
     *
     * @ORM\ManyToOne(targetEntity="Finnmark\CinemaBundle\Entity\Room")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="room_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $room;

    /**
     * @var \Finnmark\CinemaBundle\Entity\MovieVersion
     *
     * @ORM\ManyToOne(targetEntity="Finnmark\CinemaBundle\Entity\MovieVersion",inversedBy="seances")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="movie_version_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $movieVersion;

    /**
     *
     * @ORM\OneToMany(targetEntity="Booking",mappedBy="seance")
     * @var ArrayCollection
     */
    private $bookings;
    
    
    public function __construct() {
        $this->bookings = new ArrayCollection();
    }
    

    /**
     * Set at
     *
     * @param \DateTime $at
     * @return Seance
     */
    public function setAt($at)
    {
        $this->at = $at;

        return $this;
    }

    /**
     * Get at
     *
     * @return \DateTime 
     */
    public function getAt()
    {
        return $this->at;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set room
     *
     * @param \Finnmark\CinemaBundle\Entity\Room $room
     * @return Seance
     */
    public function setRoom(\Finnmark\CinemaBundle\Entity\Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \Finnmark\CinemaBundle\Entity\Room 
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set movieVersion
     *
     * @param \Finnmark\CinemaBundle\Entity\MovieVersion $movieVersion
     * @return Seance
     */
    public function setMovieVersion(\Finnmark\CinemaBundle\Entity\MovieVersion $movieVersion = null)
    {
        $this->movieVersion = $movieVersion;

        return $this;
    }

    /**
     * Get movieVersion
     *
     * @return \Finnmark\CinemaBundle\Entity\MovieVersion 
     */
    public function getMovieVersion()
    {
        return $this->movieVersion;
    }

    /**
     * Add bookings
     *
     * @param \Finnmark\CinemaBundle\Entity\Booking $bookings
     * @return Seance
     */
    public function addBooking(\Finnmark\CinemaBundle\Entity\Booking $bookings)
    {
        $this->bookings[] = $bookings;

        return $this;
    }

    /**
     * Remove bookings
     *
     * @param \Finnmark\CinemaBundle\Entity\Booking $bookings
     */
    public function removeBooking(\Finnmark\CinemaBundle\Entity\Booking $bookings)
    {
        $this->bookings->removeElement($bookings);
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBookings()
    {
        return $this->bookings;
    }
}
