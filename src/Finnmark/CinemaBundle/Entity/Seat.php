<?php

namespace Finnmark\CinemaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Seat
 *
 * @ORM\Table(name="seat", indexes={@ORM\Index(name="fk_seat_room_area1_idx", columns={"room_area_id"})})
 * @ORM\Entity(repositoryClass="Finnmark\CinemaBundle\Entity\Repository\SeatRepository")
 */
class Seat
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="map", type="text", nullable=false)
     */
    private $map;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Finnmark\CinemaBundle\Entity\RoomArea
     *
     * @ORM\ManyToOne(targetEntity="Finnmark\CinemaBundle\Entity\RoomArea",inversedBy="seats")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="room_area_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $roomArea;



    /**
     * Set name
     *
     * @param string $name
     * @return Seat
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set map
     *
     * @param string $map
     * @return Seat
     */
    public function setMap($map)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return string 
     */
    public function getMap()
    {
        return json_decode($this->map);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roomArea
     *
     * @param \Finnmark\CinemaBundle\Entity\RoomArea $roomArea
     * @return Seat
     */
    public function setRoomArea(\Finnmark\CinemaBundle\Entity\RoomArea $roomArea = null)
    {
        $this->roomArea = $roomArea;

        return $this;
    }

    /**
     * Get roomArea
     *
     * @return \Finnmark\CinemaBundle\Entity\RoomArea 
     */
    public function getRoomArea()
    {
        return $this->roomArea;
    }
}
