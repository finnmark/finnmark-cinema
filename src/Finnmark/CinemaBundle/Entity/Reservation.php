<?php

namespace Finnmark\CinemaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservation
 *
 * @ORM\Table(name="reservation", indexes={@ORM\Index(name="fk_seat_has_seance_seance1_idx", columns={"seance_id"}), @ORM\Index(name="fk_seat_has_seance_seat1_idx", columns={"seat_id"}), @ORM\Index(name="fk_reservation_booking1_idx", columns={"booking_id"})})
 * @ORM\Entity(repositoryClass="Finnmark\CinemaBundle\Entity\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @var \Finnmark\CinemaBundle\Entity\Seat
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Finnmark\CinemaBundle\Entity\Seat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="seat_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $seat;

    /**
     * @var \Finnmark\CinemaBundle\Entity\Seance
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Finnmark\CinemaBundle\Entity\Seance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="seance_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $seance;

    /**
     * @var \Finnmark\CinemaBundle\Entity\Booking
     *
     * @ORM\ManyToOne(targetEntity="Finnmark\CinemaBundle\Entity\Booking", inversedBy="reservations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="booking_id", referencedColumnName="id")
     * })
     */
    private $booking;



    /**
     * Set seat
     *
     * @param \Finnmark\CinemaBundle\Entity\Seat $seat
     * @return Reservation
     */
    public function setSeat(\Finnmark\CinemaBundle\Entity\Seat $seat)
    {
        $this->seat = $seat;

        return $this;
    }

    /**
     * Get seat
     *
     * @return \Finnmark\CinemaBundle\Entity\Seat 
     */
    public function getSeat()
    {
        return $this->seat;
    }

    /**
     * Set seance
     *
     * @param \Finnmark\CinemaBundle\Entity\Seance $seance
     * @return Reservation
     */
    public function setSeance(\Finnmark\CinemaBundle\Entity\Seance $seance)
    {
        $this->seance = $seance;

        return $this;
    }

    /**
     * Get seance
     *
     * @return \Finnmark\CinemaBundle\Entity\Seance 
     */
    public function getSeance()
    {
        return $this->seance;
    }

    /**
     * Set booking
     *
     * @param \Finnmark\CinemaBundle\Entity\Booking $booking
     * @return Reservation
     */
    public function setBooking(\Finnmark\CinemaBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \Finnmark\CinemaBundle\Entity\Booking 
     */
    public function getBooking()
    {
        return $this->booking;
    }
}
