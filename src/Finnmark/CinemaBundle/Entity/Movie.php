<?php

namespace Finnmark\CinemaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Movie
 *
 * @ORM\Table(name="movie")
 * @ORM\Entity(repositoryClass="Finnmark\CinemaBundle\Entity\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="extra", type="text", nullable=true)
     */
    private $extra;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\OneToMany(targetEntity="MovieVersion",mappedBy="movie")
     * @var ArrayCollection
     */
    protected $movieVersions;

    public function __construct() {
        $this->movieVersions = new ArrayCollection();
    }


    /**
     * Set title
     *
     * @param string $title
     * @return Movie
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set extra
     *
     * @param string $extra
     * @return Movie
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return string 
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add movieVersions
     *
     * @param \Finnmark\CinemaBundle\Entity\MovieVersion $movieVersions
     * @return Movie
     */
    public function addMovieVersion(\Finnmark\CinemaBundle\Entity\MovieVersion $movieVersions)
    {
        $this->movieVersions[] = $movieVersions;

        return $this;
    }

    /**
     * Remove movieVersions
     *
     * @param \Finnmark\CinemaBundle\Entity\MovieVersion $movieVersions
     */
    public function removeMovieVersion(\Finnmark\CinemaBundle\Entity\MovieVersion $movieVersions)
    {
        $this->movieVersions->removeElement($movieVersions);
    }

    /**
     * Get movieVersions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovieVersions()
    {
        return $this->movieVersions;
    }
}
