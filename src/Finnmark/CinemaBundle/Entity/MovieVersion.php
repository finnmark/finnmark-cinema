<?php

namespace Finnmark\CinemaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * MovieVersion
 *
 * @ORM\Table(name="movie_version", indexes={@ORM\Index(name="fk_movie_version_movie1_idx", columns={"movie_id"})})
 * @ORM\Entity(repositoryClass="Finnmark\CinemaBundle\Entity\Repository\MovieVersionRepository")
 */
class MovieVersion
{
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45, nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="length", type="integer", nullable=false)
     */
    private $length;
    
    /**
     * 
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Finnmark\CinemaBundle\Entity\Movie
     *
     * @ORM\ManyToOne(targetEntity="Finnmark\CinemaBundle\Entity\Movie", inversedBy="movieVersions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="movie_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $movie;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="Seance",mappedBy="movieVersion")
     * @var ArrayCollection
     */
    private $seances;

    public function __construct() {
        $this->seances = new ArrayCollection();
    }

    /**
     * Set type
     *
     * @param string $type
     * @return MovieVersion
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set movie
     *
     * @param \Finnmark\CinemaBundle\Entity\Movie $movie
     * @return MovieVersion
     */
    public function setMovie(\Finnmark\CinemaBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie
     *
     * @return \Finnmark\CinemaBundle\Entity\Movie 
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Add seances
     *
     * @param \Finnmark\CinemaBundle\Entity\Seance $seances
     * @return MovieVersion
     */
    public function addSeance(\Finnmark\CinemaBundle\Entity\Seance $seances)
    {
        $this->seances[] = $seances;

        return $this;
    }

    /**
     * Remove seances
     *
     * @param \Finnmark\CinemaBundle\Entity\Seance $seances
     */
    public function removeSeance(\Finnmark\CinemaBundle\Entity\Seance $seances)
    {
        $this->seances->removeElement($seances);
    }

    /**
     * Get seances
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSeances()
    {
        return $this->seances;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return MovieVersion
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set length
     *
     * @param integer $length
     * @return MovieVersion
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return integer 
     */
    public function getLength()
    {
        return $this->length;
    }
}
