<?php
namespace Finnmark\CinemaBundle\Exception\EntityConstructor;

use Finnmark\CinemaBundle\Exception\AbstractException;

class UnknownEntityName extends AbstractException {
    
}