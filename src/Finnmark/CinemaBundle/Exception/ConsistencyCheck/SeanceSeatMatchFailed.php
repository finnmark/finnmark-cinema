<?php
namespace Finnmark\CinemaBundle\Exception\ConsistencyCheck;

use Finnmark\CinemaBundle\Exception\AbstractException;

class SeanceSeatMatchFailed extends AbstractException {
    
}