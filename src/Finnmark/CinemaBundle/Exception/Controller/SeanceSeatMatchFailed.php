<?php
namespace Finnmark\CinemaBundle\Exception\Controller;

use Finnmark\CinemaBundle\Exception\AbstractException;

class SeanceNotFound extends AbstractException {
    
}