<?php
namespace Finnmark\CinemaBundle\Exception\Entity;

use Finnmark\CinemaBundle\Exception\AbstractException;

class UnknownEntityId extends AbstractException {
    
}