<?php

namespace Finnmark\CinemaTemplates\SampleTemplateBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FinnmarkCinemaTemplatesSampleTemplateBundle extends Bundle
{
    public function getParent() {
        return 'FinnmarkCinemaBundle';
    }
}
